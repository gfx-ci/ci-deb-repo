#!/usr/bin/env bash
# © David Heidelberg, Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause

export DEBIAN_FRONTEND=noninteractive

set -ex

export DEB_BUILD_OPTIONS=nocheck
export DPKG_COLORS=always

# dependencies

cp /etc/apt/sources.list.d/debian{,-src}.sources
sed -i "s/\ deb/\ deb-src/" /etc/apt/sources.list.d/debian-src.sources

# add packages build before in this repository (if exist)
if curl -s -L "https://gitlab.freedesktop.org/gfx-ci/ci-deb-repo/-/raw/${DEBIAN_DISTRO}/conf/distributions" | grep "${DEBIAN_DISTRO}"; then
	echo "deb [trusted=yes] https://gitlab.freedesktop.org/gfx-ci/ci-deb-repo/-/raw/${DEBIAN_DISTRO}/ ${DEBIAN_DISTRO} main" | tee /etc/apt/sources.list.d/gfx-ci_.list
fi

APT_CONF_FILE=/etc/apt/apt.conf.d/50build-deb-action

cat > "$APT_CONF_FILE" <<-EOF
	APT::Get::Assume-Yes "yes";
	APT::Install-Recommends "no";
	Acquire::Languages "none";
	quiet "yes";
EOF

# Adapted from pbuilder's support for cross-compilation:
if [ "$DEBIAN_ARCH" != "amd64" ] || [ "$DEBIAN_ARCH" != "arm64" ] || [ "$DEBIAN_ARCH" != "armhf" ]; then
       dpkg --add-architecture "$DEBIAN_ARCH"
       INPUT_EXTRA_BUILD_DEPS=(
         "crossbuild-essential-$DEBIAN_ARCH"
         "libc-dev:$DEBIAN_ARCH"
       )
       printf 'APT::Get::Host-Architecture "%s";\n' "$DEBIAN_ARCH" >> "$APT_CONF_FILE"
fi

apt-get -q update

apt-get install "${INPUT_EXTRA_BUILD_DEPS[@]}"

# Adapted from pbuilder's support for cross-compilation:
if [ "$DEBIAN_ARCH" != "amd64" ] || [ "$DEBIAN_ARCH" != "arm64" ] || [ "$DEBIAN_ARCH" != "armhf" ]; then
       if [ -z "${CONFIG_SITE-}" ]; then
               export CONFIG_SITE="/etc/dpkg-cross/cross-config.$DEBIAN_ARCH"
       fi
       export DEB_BUILD_OPTIONS="${DEB_BUILD_OPTIONS:+$DEB_BUILD_OPTIONS }nocheck"
       export DEB_BUILD_PROFILES="${DEB_BUILD_PROFILES:+$DEB_BUILD_PROFILES }cross nocheck"
       INPUT_BUILDPACKAGE_OPTS=(
         "--host-arch=$DEBIAN_ARCH"
       )
fi

pushd "$PKG_NAME"
  # Set the version based on the checked out tag that contain at least on digit
  # strip any leading non digits as they are not part of the version number
  git_tag=$(git describe --tag  --match "*[0-9]*" 2>/dev/null || echo -n 0)
  new_tag=$(echo -n "$git_tag" | sed -e 's@^[^0-9]*@@' -e 's@_@~@g')-$(date +%Y.%m.%d | tr -d '\n')
  sed -i "1 s@([^)]*)@($new_tag)@" debian/changelog

  # shellcheck disable=SC2086
  apt-get build-dep -y ${PKG_NAME:?} -- "./"

  dpkg-buildpackage -b -uc -us "${INPUT_BUILDPACKAGE_OPTS[@]}" 2>&1 1>/dev/null
popd
